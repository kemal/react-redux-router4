import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { registerUser } from '../../actions/auth';
import { Link } from 'react-router-dom'

const form = reduxForm({
  form: 'register',

});


const renderField = field => (
  <div>
    <input className="form-control" {...field.input} />
    {field.touched && field.error && <div className="error">{field.error}</div>}
  </div>
);

class Register extends Component {
  handleFormSubmit(formProps) {
    this.props.registerUser(formProps);
  }

  validation = () => {
    if(this.props.errors){
      const errors = this.props.errors
      const keys = Object.keys(errors);
      const msg = [];
      keys.forEach((item, index, array) => msg.push(errors[item][0]))
      return msg
    }
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="row">
          <div className="col-md-8 offset-md-2">
        <div className="card">
            <div className="card-header">Create an account!</div>

                  <div className="row">     
                    <div className="col-md-10 offset-md-1">

      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))} className="form-horizontal">
        <p>{this.validation()}</p>
        
        <div className="form-group row">
            <label className="col-md-4 col-form-label">Company Name</label>
            <div className="col-md-6">
              <Field name="company" className="form-control" component={renderField} type="text" />
             </div>
        </div>

        <div className="form-group row">
            <label className="col-md-4 col-form-label">Name</label>
            <div className="col-md-6">
              <Field name="name" className="form-control" component={renderField} type="text" />
            </div>
          </div>
        
        <div className="form-group row">
          
            <label className="col-md-4 col-form-label">Email</label>
            <div className="col-md-6">
              <Field name="email" className="form-control" component={renderField} type="text" />
            </div>
          </div>
        
        <div className="form-group row">
          
            <label className="col-md-4 col-form-label">Password</label>
            <div className="col-md-6">
              <Field name="password" className="form-control" component="input" type="password" />
            </div>  
          </div>
        
        <div className="form-group row">
          
            <label className="col-md-4 col-form-label">Confirm Password</label>
            <div className="col-md-6">
              <Field name="password_confirmation" className="form-control" component="input" type="password" />
            </div>  
          </div>
         
        <div className="form-group row">
              <div className="col-md-8 offset-md-4">
                  <button type="submit" className="btn btn-info">
                      Register
                  </button>
              </div>
          </div>         
       </form>
      </div>       
      </div>       
      </div>       
      		<p className="text-center">
           <Link to="/login" className="btn btn-primary" >Login to your account!</Link>
 				  </p>
      </div>
          </div>      



    );
  }
}

function mapStateToProps(state) {
  return {
    message: state.auth.error,
    errors: state.auth.error.errors,
    authenticated: state.auth.authenticated
  };
}

export default connect(mapStateToProps, { registerUser })(form(Register));