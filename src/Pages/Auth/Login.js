import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { loginUser } from '../../actions/auth';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'

  const form = reduxForm({
    form: 'login',
    fields: ['email', 'password'],
  });

  class Login extends Component {
 
    handleSubmit = (formProps) => {
      this.props.loginUser(formProps);
    }

    validation = () => {
      if(this.props.errors){
        const errors = this.props.errors
        const keys = Object.keys(errors);
        const msg = [];
        keys.forEach((item, index, array) => msg.push(errors[item][0]))
        return msg
      }
    }

    render(){
      return (
        <div className="row">
          <div className="col-md-8 offset-md-2">
            <div className="card">
                <div className="card-header">Login</div>
                  <div className="row">     
                    <div className="col-md-10 offset-md-1">


              <form onSubmit={this.props.handleSubmit(this.handleSubmit.bind(this))}>
          <p>{this.validation()}</p>
           <div className="form-group row">
            <label className="col-md-4 col-form-label">Email</label>
            <div className="col-md-6">
              <Field name="email" component="input"  type="text" className="form-control" />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-md-4 col-form-label">Password</label>
            <div className="col-md-6">
              <Field name="password" component="input" type="password"  className="form-control" />
            </div>
          </div>
          <div className="form-group">
              <div className="col-md-8 col-md-offset-4">
                  <button type="submit" className="btn btn-info">
                      Login
                  </button>
                  <Link to="/password/reset" className="btn btn-link" >Forgot Your Password?</Link>
              </div>
          </div>
        </form>
        </div>       </div>       
      </div>       
      <br/>
      		<p className="text-center">
           <Link to="/register" className="btn btn-primary" >Sign Up for a new account!</Link>
 				  </p>
      </div>
          </div>      

      )
    }
  }
 
export default connect(
  (state) => ({    
    message: state.auth.error,
    errors: state.auth.error.errors,
    authenticated: state.auth.authenticated}),
  {loginUser }
)(form(Login))