import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom'
import App from './app/App'
import cookie from 'react-cookies';
import registerServiceWorker from './registerServiceWorker';

import './styles/index.css'
import 'jquery';


import store from './store'
import { AUTH_USER } from './constants';

const token = cookie.load('token')
if (token) {
  // Update application state. User has token and is probably authenticated
  store.dispatch({ type: AUTH_USER });
} 

ReactDOM.render((
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
), document.getElementById('root'))

 
  registerServiceWorker();