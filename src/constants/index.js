//= =====================
// Auth Actions
//= =====================
export const AUTH_USER = 'auth_user',
  UNAUTH_USER = 'unauth_user',
  AUTH_ERROR = 'auth_error',
  FORGOT_PASSWORD_REQUEST = 'forgot_password_request',
  RESET_PASSWORD_REQUEST = 'reset_password_request',
  PROTECTED_TEST = 'protected_test';

//= =====================
// User Profile Actions
//= =====================
export const FETCH_USER = 'fetch_user';


//= =====================
// API URL
//= =====================
export const API_URL = 'http://api.sinemateknik.com/v1';
 
//= =====================
// CLIENT URL
//= =====================
export const CLIENT_ROOT_URL = 'http://localhost:3000';
export const IMAGE_URL = `http://backoffice.sinemateknik.com/images/movies/`;

export const PERSON_MOVIE_CREDITS = 'PERSON_MOVIE_CREDITS';
export const GENRE_FILMS = 'GENRE_FILMS';
export const GENRES = 'GENRES';
export const GENRE = 'GENRE';

export const PERSON_PAGE = 'PERSON_PAGE';
export const MOVIE_PAGE = 'MOVIE_PAGE';
export const NOW_PLAYING = 'NOW_PLAYING';
export const COMING_SOON = 'COMING_SOON';
export const NEW_MOVIES = 'NEW_MOVIES';
export const MOVIES_LIST = 'MOVIES_LIST';
export const MOVIE_CREDITS = 'MOVIE_CREDITS';
export const SEARCH_RESULTS = 'SEARCH_RESULTS';
export const CATEGORY_FILMS = 'CATEGORY_FILMS';
export const CATEGORY = 'CATEGORY';
export const CATEGORIES = 'CATEGORIES';