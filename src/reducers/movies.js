import * as constants from '../constants';

const INITIAL_STATE = { list: [] };

export default function(state = INITIAL_STATE, action){
    switch (action.type) {
      case constants.MOVIES_LIST:
        return {...state, list: action.payload };
      default:
        return state;
    }
}