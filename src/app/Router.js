import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../Pages/Home'
import Secret from '../Pages/Secret'
import Movies from '../Pages/Movies'
import NoMatch from '../Pages/NoMatch'
import Login from '../Pages/Auth/Login'
import Register from '../Pages/Auth/Register'
import Logout from '../Pages/Auth/Logout'
import RequireAuth from '../components/require_auth'; 

const Router = () => (
    <Switch>
      <Route exact path="/" component={RequireAuth(Home)}/>
      <Route path="/movies" component={Movies}/>
      <Route path="/secret" component={RequireAuth(Secret)}/>        
      <Route path="/login" component={Login}/>
      <Route path="/Logout" component={Logout}/>
      <Route path="/register" component={Register}/> 
      <Route path="/logout" component={Logout} />
      <Route path="*"  component={NoMatch}/>  
    </Switch>
)
export default Router