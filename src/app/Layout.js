import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Router from './Router'
import Header from './Header'
import Footer from './Footer'

import './layout.css'

const Layout = ({ children }) => (
  <div>
    <Helmet
      title="CP"
      meta={[
        { name: 'description', content: '' },
        { name: 'keywords', content: '' },
      ]}
      script={[
        { 'src': 'https://use.fontawesome.com/releases/v5.0.4/js/all.js'},
      ]}
      link={[
        {'rel':'stylesheet', 'href': 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'}
      ]}
    />
    <Header />
     {/* {children()} */}
       <section className="container main">
        <Router />
      </section>
     <Footer />
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout