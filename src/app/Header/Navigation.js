import React, {Component} from 'react'
import { connect } from 'react-redux';
import {
    Navbar,
    NavbarBrand,     
    Collapse,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
  
class Navigation extends Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }

    render(){
    if(this.props.authenticated === true) {
        return(
            <Navbar expand="md">
            <div className="container">
                <NavbarBrand href="/">reactstrap</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink href="/monitor/filter">Filter</NavLink>
                    </NavItem>
                    <NavItem> 
                        <NavLink href="/secret">Secret</NavLink>   
                    </NavItem>                        
                    <NavItem>
                        <NavLink href="/movies">Movies</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/components/">Components</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                        Options
                        </DropdownToggle>
                        <DropdownMenu right>
                        <DropdownItem>
                            Option 1
                        </DropdownItem>
                        <DropdownItem>
                            Option 2
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem>
                            Reset
                        </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                    <NavItem>
                        <NavLink href="/logout">Logout</NavLink>
                    </NavItem>

                </Nav>
                </Collapse>
            </div>
            </Navbar>    
        )
      } else {
        return(
            <Navbar expand="md">
            <div className="container">
            <NavbarBrand href="/">reactstrap</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="/login">Login</NavLink>
                        </NavItem>
                        <NavItem> 
                            <NavLink href="/register">Register</NavLink>   
                        </NavItem>                        
                    </Nav>
                </Collapse>
            </div>
            </Navbar>
            )
        }
    }
}
export default connect(
    (state) => ({authenticated: state.auth.authenticated}),
  )(Navigation)
  