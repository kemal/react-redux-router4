import React from 'react'

const Footer = () => (
    <footer>
        <p className="text-center">
            &copy; 2018
        </p>
    </footer>
)
export default Footer