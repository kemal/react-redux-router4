import axios from 'axios';
import { errorHandler } from './index';
import * as constants from '../constants';


export function getMovies(page=1) {
  return function (dispatch) {
    axios.get(`${constants.API_URL}/movies?page=${page}`)
    .then((response) => {
      dispatch({
        type: constants.MOVIES_LIST,
        payload: response.data.data,
      });
    })
    .catch(response => dispatch(errorHandler(response.data.error)));
  };
}
