
import { logoutUser } from './auth';

export function errorHandler(dispatch, error, type) {
  let errorMessage = error.response ? error.response.data : error;

    // NOT AUTHENTICATED ERROR
  if (error.status === 401 || error.response.status === 401) {
    errorMessage = 'You are not authorized to do this.';
    return dispatch(logoutUser(errorMessage));
  }

  dispatch({
    type,
    payload: errorMessage,
  });
}