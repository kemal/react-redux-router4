import {createStore, applyMiddleware, combineReducers} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import { reducer as formReducer } from 'redux-form'

import thunk from 'redux-thunk'
import movies from './reducers/movies'
import auth from './reducers/auth'

const reducer = combineReducers({
    form: formReducer,
    movies,
    auth 
}) 

export default createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
)