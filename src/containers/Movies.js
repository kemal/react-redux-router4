import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMovies } from '../actions/movies';

class Movies extends Component {

  componentDidMount(){
      this.props.getMovies();
  }

  listMovies() {
    return(
      this.props.movies.map((movie, index) => {
        return (
            <li key={index}>{movie.title}</li>
          );
      })
    )
  }

  render() {
    if(!this.props.movies){
        return (<div>Loading ...</div>)
    }
   return (
      <div>
          <ul>
            {this.listMovies()}
          </ul>
    </div>
    );
  }
}

export default connect(
  (state) => ({movies: state.movies.list}),
  {getMovies}
)(Movies)